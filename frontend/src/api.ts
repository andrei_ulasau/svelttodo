import { todoListStore } from "./store";
import { v4 as uuid } from "uuid";

const baseUrl = "http://localhost:3000/todos";

export async function getTodos() {
  const getTodos = await fetch(baseUrl);

  const json = await getTodos.json();

  todoListStore.set(json);
}

export async function createTodo({ title, description, deadline }) {
  try {
    await fetch(baseUrl, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        title,
        description,
        deadline,
        isDone: false,
      }),
    });

    await getTodos();
  } catch (error) {
    console.log("create error ", error.message);
  }
}

export async function updateTodoById({
  _id,
  title,
  description,
  deadline,
  isDone,
}) {
  try {
    await fetch(`${baseUrl}/${_id}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        title,
        description,
        deadline,
        isDone,
      }),
    });

    await getTodos();
  } catch (error) {
    console.log("update error ", error.message);
  }
}

export async function deleteTodoById(todo) {
  try {
    await fetch(`${baseUrl}/${todo._id}`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
    });

    await getTodos();
  } catch (error) {
    console.log("delete error ", error.message);
  }
}
