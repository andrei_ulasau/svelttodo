import { writable } from "svelte/store";

const mockedTodolist = [
  {
    id: "123123",
    title: "Write my first post",
    description: "description",
    deadline: "deadline",
    isDone: true,
  },
  {
    id: "523532",
    title: "Upload the post to the blog",
    description: "description",
    deadline: "deadline",
    isDone: false,
  },
];

export const todoListStore = writable([]);
