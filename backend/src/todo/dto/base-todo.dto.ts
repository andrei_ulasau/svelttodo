export class TodoDto {
  title: string;
  description?: string;
  deadline?: string;
  isDone?: boolean;
}
