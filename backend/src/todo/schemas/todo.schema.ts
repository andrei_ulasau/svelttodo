import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type TodoDocument = Todo & Document;

@Schema()
export class Todo {
  @Prop()
  title: string;

  @Prop()
  description?: string;

  @Prop()
  deadline?: string;

  @Prop()
  isDone?: boolean;

  @Prop({default: new Date()})
  createdAt?: Date;
}

export const TodoSchema = SchemaFactory.createForClass(Todo);
