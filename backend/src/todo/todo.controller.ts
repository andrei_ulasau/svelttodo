import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { TodoService } from './todo.service';
import { TodoDto } from './dto/base-todo.dto';

@Controller('todos')
export class TodoController {
  constructor(private readonly service: TodoService) {}

  @Get()
  async getAllTodos() {
    return await this.service.findAll();
  }

  @Get(':id')
  async getTodoById(@Param('id') id: string) {
    return await this.service.findOne(id);
  }

  @Post()
  async createTodo(@Body() createTodoDto: TodoDto) {
    console.log({ createTodoDto });
    return await this.service.create(createTodoDto);
  }

  @Put(':id')
  async updateTodo(@Param('id') id: string, @Body() updateTodoDto: TodoDto) {
    return await this.service.update(id, updateTodoDto);
  }

  @Delete(':id')
  async deleteTodo(@Param('id') id: string) {
    return await this.service.delete(id);
  }
}
